import firebase from "firebase/app";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyDWzNvuK_51x3hHuycO9JMlpTq1h9YaBt8",
  authDomain: "bc-dates.firebaseapp.com",
  databaseURL: "https://bc-dates.firebaseio.com",
  projectId: "bc-dates",
  storageBucket: "bc-dates.appspot.com",
  messagingSenderId: "716836562188",
  appId: "1:716836562188:web:687afebb1bf9657a295529",
  measurementId: "G-XV59CQW1TG"
};

//create new firestore app if one not already created
let app = null;
app = firebase.apps.length
  ? firebase.app()
  : firebase.initializeApp(firebaseConfig);

export const db = app.firestore();
