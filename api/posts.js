import GhostContentAPI from "@tryghost/content-api";

const api = new GhostContentAPI({
  // url: "http://167.71.181.156",
  url: "https://bcdates.ddns.net",
  key: "800beedd57e37c91d6023da7dd",
  version: "v3"
});

export async function getPosts() {
  return await api.posts
    .browse({
      include: "tags,authors",
      limit: "all"
    })
    .catch(err => console.log(err));
}

export async function getSinglePost(postId) {
  return await api.posts
    .read({
      id: postId
    })
    .catch(err => {
      console.log(err);
    });
}
