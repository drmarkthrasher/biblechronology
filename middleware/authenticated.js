export default function({ store, route, redirect }) {
  const user = store.state.modules.auth.user;
  let protectedRoute = false;

  //put all protected routes here
  if (route.path == "/" || route.path == "/posts" || route.path == "/about") {
    protectedRoute = true;
  }

  if (!user && protectedRoute) {
    redirect("/auth/login");
  }
}
