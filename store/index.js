import JWTDecode from "jwt-decode";
import cookieparser from "cookieparser";

import { getPosts } from "~/api/posts";

export const state = () => ({
  counter: 5
});

export const getters = {};

export const actions = {
  async nuxtServerInit({ commit }, { req }) {
    //this function is called whenever the servier is initialized
    //put authentication stuff here
    if (process.server && process.static) return;
    if (!req.headers.cookie) return;

    const parsed = cookieparser.parse(req.headers.cookie);
    const accessTokenCookie = parsed.access_token;
    if (!accessTokenCookie) return;

    const decoded = JWTDecode(accessTokenCookie);

    if (decoded) {
      commit("modules/auth/setAuthUser", {
        uid: decoded.user_id,
        email: decoded.email
      });
    }

    //get posts from ghost cms blog backend and put in store
    const posts = await getPosts();
    this.dispatch("modules/posts/setPosts", posts);
  }
};

export const mutations = {
  increment(state) {
    state.counter++;
  }
};
