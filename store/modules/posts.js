// import { db } from '@/db';
import firebase from "firebase/app";
import "firebase/auth";

export const state = () => ({
  posts: null,
  post: null
});

export const getters = {};

export const actions = {
  async setPosts({ commit }, posts) {
    try {
      commit("setPosts", posts);
    } catch {}
  },
  async setSinglePost({ commit }, post) {
    try {
      commit("setSinglePost", post);
    } catch {}
  }
};

export const mutations = {
  setPosts(state, posts) {
    state.posts = posts;
  },
  setSinglePost(state, post) {
    state.post = post;
  }
};
