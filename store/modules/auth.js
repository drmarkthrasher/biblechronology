import { db } from "~/plugins/firebase";
import firebase from "firebase/app";
import "firebase/auth";
import Cookie from "js-cookie";

export const state = () => ({
  user: null
});

export const getters = {
  isAuthenticated(state) {
    return !!state.user;
  }
};

export const actions = {
  async register({ commit }, { firstname, lastname, email, password }) {
    try {
      await firebase.auth().createUserWithEmailAndPassword(email, password);
      const user = firebase.auth().currentUser;
      const token = await user.getIdToken(true);

      Cookie.set("access_token", token);

      //note that user here is an observer, need to convert it
      let userobj = JSON.parse(JSON.stringify(user));
      commit("setAuthUser", userobj);
      return "mystring";
    } catch (error) {
      throw error;
    }
  },
  async login({ commit }, { email, password }) {
    try {
      await firebase.auth().signInWithEmailAndPassword(email, password);
      const user = firebase.auth().currentUser;
      const token = await user.getIdToken(true);

      Cookie.set("access_token", token);

      //note that user here is an observer, need to convert it
      let userobj = JSON.parse(JSON.stringify(user));
      commit("setAuthUser", userobj);

      // use return here if want to return any data to calling function...
      return "mystring";
    } catch (error) {
      throw error;
    }
  },
  async logout({ commit }) {
    await firebase.auth().signOut();
    commit("setAuthUser", null);
    this.$router.push("auth/login");
  }
};

export const mutations = {
  setAuthUser(state, data) {
    state.user = data;
  }
};
